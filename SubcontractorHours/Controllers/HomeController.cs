﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Data.SqlClient;
using System.Configuration;
using PagedList;
using PagedList.Mvc;
using System.IO;
using System.Net.Http;
using System.Web.Security;
using SubcontractorHours.Models;

namespace SubcontractorHours.Controllers
{
    public class HomeController : KPIController
    {

        private App_KPI_SubcontractorHoursEntities db = new App_KPI_SubcontractorHoursEntities();
        private App_KPI_SubcontractorHoursEntitiesSub dbjobs = new App_KPI_SubcontractorHoursEntitiesSub();

        [HttpGet]
        public ActionResult Jobs(string SHMCU, int? SISubcontractor)
        {
            dynamic showMessageString = string.Empty;


            if (SHMCU.Length > 12)
            {
                SHMCU = SHMCU.Substring(SHMCU.Length - 12, 12);

            }
            string z;

            if (SISubcontractor == null)
            {
                showMessageString = new
                {
                    param1 = 200,
                    param2 = ""

                };

            }
            else
            {
                try
                {
                    z = dbjobs.spSubContractorsAuthJob(SHMCU, SISubcontractor).Select(q => q.abalph).FirstOrDefault();
                    if (z == null)
                    {
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = "No Contractors found with that number"
                        };
                    }
                    else
                    {
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = z
                        };
                    }

                }
                catch (Exception ex)
                {
                    showMessageString = new
                    {
                        param1 = 404,
                        param2 = "No Contractors found with that number"
                    };


                };

            }




            return Json(showMessageString, JsonRequestBehavior.AllowGet);




        }

        [HttpPost]
        public ActionResult Jobs(string searchString, string SHMCU)
        {

            var q = dbjobs.spSubContractorsAuthJob(SHMCU, null).ToList();
            if (searchString != null)
            {
                if (searchString.Trim() != "")
                {
                    q = q.Where(s => s.PHAN8.ToString().Contains(searchString.ToLower()) || s.abalph.ToLower().Contains(searchString.ToLower())).ToList();
                }
            }

            return View(q);


        }

        [HttpGet]
        [Authorize]
        public ActionResult Default()
        {

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }



            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View(new List<spSubcontractorHours_Result>());
            }

            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
            short myshort = short.Parse(TempData.Peek("KeyType").ToString());


            var requisitions = db.spSubcontractorHours(myshort, myid, null, null, false).Select(u => new spSubcontractorHours_Result { SHRequisitionDate = u.SHRequisitionDate, ENDescription = u.ENDescription, MMRegion = u.MMRegion, jobdescription = u.jobdescription, MMMCU = u.MMMCU, MMRegionGroup = u.MMRegionGroup, MMKeyType = u.MMKeyType, SHTotalHours = u.SHTotalHours, SHTotalHoursJTD = u.SHTotalHoursJTD, SHStatus = u.SHStatus, SHDateChange = u.SHDateChange,QCF03K = u.QCF03K }).Where(u => u.QCF03K == false).ToList();

            var req2 = requisitions.Where(r => r.MMRegionGroup != null).Select(r => r.MMRegionGroup).Distinct().OrderBy(x => x);
            var req1 = requisitions.Where(r => r.MMRegion != null).Select(r => r.MMRegion).Distinct().OrderBy(x => x);
            var req3 = req2.Concat(req1);

            var myregions = new SelectList(req3.ToList());
            ViewBag.Regions = myregions;

            //var companies = new SelectList(dbCompany.spSubcontractorHoursCompany(myshort, myid).Select(u => new { Text = u.ENDescription, Value = u.ENKeyType }).OrderBy(u => u.Value), "Value", "Text").ToList();
           //  var companies = requisitions.Where(r => r.ENDescription != null).Select(u => new { Text = u.ENDescription, Value = u.MMKeyType }).Distinct().OrderBy(x => x).ToList();
            var companies = new SelectList(requisitions.Select(u => new { Text = u.ENDescription, Value = u.MMKeyType }).OrderBy(u => u.Value).Distinct(), "Value", "Text").ToList();

            ViewBag.Companies = companies;
            ViewBag.Companycount = companies.Count();

            
            int numreq = requisitions.Count;

            // SORTING

            // PAGING STUFF

            int pageSize = 25;
            int pageNumber = 1;
            decimal mypgct = numreq / Convert.ToDecimal(pageSize);
            int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
            ViewBag.mypagecount = mypagecount;

            var list = new List<SelectListItem>();

            for (int i = 1; i <= mypagecount; i++)
                list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

            ViewBag.list = list;
            ViewBag.pagenum = pageNumber.ToString();

            return View(requisitions.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            if (CSInclude.SSO_Check_LocalIP(Request.ServerVariables["REMOTE_ADDR"].ToString()) != 0)
            {
                if (Request.QueryString["ReturnUrl"] != null || Request.QueryString["ReturnID"] != null)
                {
                    if (Request.QueryString["ReturnID"] == null)
                    {
                        string SingleSignOnURL = System.Configuration.ConfigurationManager.AppSettings["KPISingleSignOnURL"].ToString();
                        string ReturnID = CSInclude.SSO_Create_Ticket(Request.ServerVariables["REMOTE_ADDR"].ToString(), CSInclude.Build_ApplicationPath(true)).ToString();
                        Response.Redirect(SingleSignOnURL + "?ReturnID=" + ReturnID + "&ReturnPath=" + CSInclude.Build_ApplicationPath(false));
                        Response.End();
                    }
                    else
                    {
                        string RtnKey = CSInclude.SSO_Delete_Ticket(Request.QueryString["ReturnID"]).ToString();
                        if (RtnKey != "")
                        {
                            FormsAuthentication.RedirectFromLoginPage(RtnKey, false);
                            Response.End();
                        }
                    }
                }
            }

            // If server variables are missing or there is no "ReturnURL" or "ReturnID" in the querystring, then something is wrong.
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public ActionResult NoAccess(string MsgID)
        {
            ViewBag.Error = MsgID;
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Default(string myaction, string MMMCU, DateTime? SHRequisitionDate, string referrer, string buttontype, string searchString, string Regions, short? Companies, string sortOrder, string currentFilter, string currentFilterR, string currentFilterC, string MyPagedList, string whichpage, string whichpagenum, int? SISubcontractor, decimal? SIHours, string SILowerTierSub, bool? chkkahua)
        {
            
            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }



            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View(new List<spSubcontractorHours_Result>());
            }

            Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
            short myshort = short.Parse(TempData.Peek("KeyType").ToString());

            ViewBag.chkkahua = chkkahua;
            
            if (myaction == "Default")
            {
                if (String.IsNullOrEmpty(searchString))
                {
                    searchString = currentFilter;
                }
                if (Companies == null)
                {
                    Companies = Convert.ToInt16(currentFilterC);
                    if (Companies == 0)
                    {
                        Companies = null;
                    }
                }
                if (String.IsNullOrEmpty(Regions))
                {
                    Regions = currentFilterR;
                }
               

                ViewBag.CurrentSort = sortOrder;
                ViewBag.CurrentFilter = searchString;
                ViewBag.CurrentFilterR = Regions;
                ViewBag.CurrentFilterC = Companies;


                IEnumerable<spSubcontractorHours_Result> requisitions;
                if (chkkahua == true)
                {
                    requisitions = db.spSubcontractorHours(myshort, myid, null, null, false).AsQueryable().ToList();

                } else
                {
                    requisitions = db.spSubcontractorHours(myshort, myid, null, null, false).Where(u => u.QCF03K == false).AsQueryable().ToList();
                }

                var companies = new SelectList(requisitions.Select(u => new { Text = u.ENDescription, Value = u.MMKeyType }).OrderBy(u => u.Value).Distinct(), "Value", "Text").ToList();

                if (!String.IsNullOrEmpty(searchString))
                {
                    requisitions = requisitions.Where(s => s.jobdescription.ToLower().Contains(searchString.ToLower()));
                }

                if (Companies != null)
                {
                    requisitions = requisitions.Where(s => s.MMKeyType.Equals(Companies));
                    var req2 = requisitions.Where(r => r.MMRegionGroup != null).Select(r => r.MMRegionGroup).Distinct().OrderBy(x => x);
                    var req1 = requisitions.Where(r => r.MMRegion != null).Select(r => r.MMRegion).Distinct().OrderBy(x => x);
                    var req3 = req2.Concat(req1);


                    if (Regions != null && req3.Cast<string>().Contains(Regions) == false)
                    {
                        Regions = null;
                    }
                    

                    var myregions = new SelectList(req3.ToList(), Regions);
                    ViewBag.Regions = myregions;
                }
                else
                {
                    var req2 = requisitions.Where(r => r.MMRegionGroup != null).Select(r => r.MMRegionGroup).Distinct().OrderBy(x => x);
                    var req1 = requisitions.Where(r => r.MMRegion != null).Select(r => r.MMRegion).Distinct().OrderBy(x => x);
                    var req3 = req2.Concat(req1);

                    var myregions = new SelectList(req3.ToList(), Regions);
                    ViewBag.Regions = myregions;
                }
                if (!String.IsNullOrEmpty(Regions))
                {
                    requisitions = requisitions.Where(s => s.MMRegion.Contains(Regions) || (s.MMRegionGroup != null && s.MMRegionGroup.Contains(Regions)));
                }






                // var companies = new SelectList(dbCompany.spSubcontractorHoursCompany(myshort, myid).Select(u => new { Text = u.ENDescription, Value = u.ENKeyType }).OrderBy(u => u.Value), "Value", "Text").ToList();
                
                ViewBag.Companies = companies;
                ViewBag.Companycount = companies.Count();

                //SEARCH FILTERS

                

            

                int numreq = requisitions.Count();

                // SORTING

                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "jobdescrption_desc" : "";
                ViewBag.DateSortParm = sortOrder == "SHRequisitionDate" ? "SHRequisitionDate_desc" : "SHRequisitionDate";
                
                switch (sortOrder)
                {
                    case "jobdescrption_desc":
                        requisitions = requisitions.OrderByDescending(s => s.jobdescription);
                        break;
                    case "SHRequisitionDate":
                        requisitions = requisitions.OrderBy(s => s.SHRequisitionDate);
                        break;
                    case "SHRequisitionDate_desc":
                        requisitions = requisitions.OrderByDescending(s => s.SHRequisitionDate);
                        break;
                    default:
                        requisitions = requisitions.OrderBy(s => s.jobdescription);
                        break;
                }

                // PAGING STUFF

                int pageSize = 25;
                int pageNumber = 1;
                decimal mypgct = numreq / Convert.ToDecimal(pageSize);
                int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
                ViewBag.mypagecount = mypagecount;
                if (MyPagedList != "" && MyPagedList != null)
                {
                    pageNumber = Convert.ToInt32(MyPagedList);
                }
                else if (whichpagenum != "" && whichpagenum != null)
                {
                    pageNumber = Convert.ToInt32(whichpagenum);
                }

                if (pageNumber > mypagecount)
                {
                    pageNumber = 1;
                }

                switch (whichpage)
                {
                    case "Next":
                        pageNumber++;
                        break;
                    case "Last":
                        pageNumber = mypagecount;
                        break;
                    case "Previous":
                        pageNumber--;
                        break;
                    case "First":
                        pageNumber = 1;
                        break;
                }

                var list = new List<SelectListItem>();

                for (int i = 1; i <= mypagecount; i++)
                    list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

                ViewBag.list = list;
                ViewBag.pagenum = pageNumber.ToString();

                return View(requisitions.ToPagedList(pageNumber, pageSize));
                // return View(requisitions);
            }
            else if (myaction == "Index2")
            {


                while (MMMCU.Length < 12)
                {
                    MMMCU = " " + MMMCU;

                }

                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, null, false).ToList();
                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }


            }
            else if (myaction == "Details")
            {
                if (MMMCU == null || SHRequisitionDate == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    while (MMMCU.Length < 12)
                    {
                        MMMCU = " " + MMMCU;
                    }
                    List<spSubcontractorHours_Result> OB2 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                    if (OB2 == null)
                    {
                        return HttpNotFound();
                    }
                    if (OB2.Count == 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        return View(OB2);
                    }


                }
            }
            else if (myaction == "Insert")
            {
                dynamic showMessageString = string.Empty;

                using (var con = new SqlConnection

                        (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                {

                    var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                    cmd.Parameters.Add(new SqlParameter("@SISubcontractor", SqlDbType.Int)).Value = SISubcontractor;

                    cmd.Parameters.Add(new SqlParameter("@SILowerTierSub", SqlDbType.VarChar)).Value = SILowerTierSub;

                    cmd.Parameters.Add(new SqlParameter("@SIHours", SqlDbType.Decimal)).Value = SIHours;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 81;


                    try
                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "success"

                        };

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }


                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);



            }
            else if (myaction == "Delete")
            {

                dynamic showMessageString = string.Empty;
                using (var con = new SqlConnection

                (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                {

                    var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;
                    cmd.Parameters.Add(new SqlParameter("@SISubcontractor", SqlDbType.Int)).Value = SISubcontractor;
                    cmd.Parameters.Add(new SqlParameter("@SILowerTierSub", SqlDbType.VarChar)).Value = SILowerTierSub;
                    cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;
                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();
                    if (referrer == "Edit")
                    {
                        cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 83;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 80;
                    }

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new

                        {
                            param1 = 200,
                            param2 = "row-" + SHRequisitionDate
                        };
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;
                        var errorMsg = ex.Message.ToString();
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally
                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }
                }

                if (referrer == "Edit")
                {
                    List<spSubcontractorHours_Result> r2 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                    if (r2.Count == 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        return View(r2);
                    }


                }
                else if (referrer == "Details")
                {
                    List<spSubcontractorHours_Result> r2 = db.spSubcontractorHours(myshort, myid, MMMCU, null, false).ToList();
                    if (r2.Count == 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        return View(r2);
                    }


                }
                else
                {
                    return Json(showMessageString, JsonRequestBehavior.AllowGet);
                }

            }
            else if (myaction == "CreateNew")
            {

                short result = 0;
                ViewBag.Message = null;

                dynamic showMessageString = string.Empty;
                using (var con = new SqlConnection

                (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                {

                    var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 60;

                    var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);

                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();

                        result = Convert.ToInt16(returnParameter.Value);

                        showMessageString = new

                        {
                            param1 = 200,
                            param2 = SHRequisitionDate.Value.ToShortDateString()
                        };

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;
                        var errorMsg = ex.Message.ToString();
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };
                    }


                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }


                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            else if (myaction == "Edit")
            {
                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();

                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }






            }
            else if (myaction == "ReopenPending")
            {
                dynamic showMessageString = string.Empty;

                using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                {

                    var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                    cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = 0;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 72;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Pending"

                        };
                    }
                    catch (Exception ex)
                    {
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }

                }


                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }



            }
            else if (myaction == "PencilMonth")
            {
                dynamic showMessageString = string.Empty;

                using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                {

                    var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                    cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = 10;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 72;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Pencil Submitted"

                        };

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = ex.Message;
                        showMessageString = new
                        {

                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }

                }

                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }



            }
            else
            {
                return View();
            }
        }


        [HttpPost]
        [Authorize]
        public ActionResult Edit(IList<SubcontractorHours.Models.spSubcontractorHours_Result> model, string MMMCU, DateTime SHRequisitionDate, string SHRemark, string referrer, string buttontype, int? SISubcontractor, decimal? SIHours, string abalph, string SILowerTierSub)
        {

            Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
            short myshort = short.Parse(TempData.Peek("KeyType").ToString());
            if (referrer == "Index2" || referrer == "Details")
            {

                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }



            }
            else
            {

                dynamic showMessageString = string.Empty;

                switch (buttontype)
                {


                    case "Cancel":
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Cancelled"

                        };
                        break;
                    case "Save":
                    case "Submit Pencil Draft":
                    case "Submit Final":
                        using (var con = new SqlConnection (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                        {

                            var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                            cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                            if (buttontype == "Submit Pencil Draft")
                            {
                                cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = 10;
                            }
                            else if (buttontype == "Submit Final")
                            {
                                cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = 20;
                            }
                            else
                            {
                                cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = DBNull.Value;
                            }

                            cmd.Parameters.Add(new SqlParameter("@SHRemark", SqlDbType.VarChar)).Value = SHRemark;

                            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                            cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 70;

                            try
                            {

                                if (con.State != ConnectionState.Open)

                                    con.Open();

                                cmd.ExecuteNonQuery();
                                showMessageString = new
                                {
                                    param1 = 200,
                                    param2 = "success"

                                };

                            }
                            catch (Exception ex)
                            {
                                ViewBag.Message = ex.Message;
                                showMessageString = new
                                {
                                    param1 = 404,
                                    param2 = ex.Message
                                };

                            }

                            finally

                            {

                                if (con.State != ConnectionState.Closed)

                                    con.Close();

                            }

                        }

                        if (ViewBag.Message == null )
                        {

                            if (model != null)
                            {

                            

                            foreach (spSubcontractorHours_Result item in model)
                            {
                                if (item.SILowerTierSubOld != item.SILowerTierSub || item.SIHours != item.SIHoursOld || item.SIHours == 0)
                                {

                                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))
                                    {

                                        var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                                        cmd.CommandType = CommandType.StoredProcedure;

                                        cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = item.MMMCU;

                                        cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = item.SHRequisitionDate;

                                        cmd.Parameters.Add(new SqlParameter("@SISubcontractor", SqlDbType.Int)).Value = item.SISubcontractor;

                                        cmd.Parameters.Add(new SqlParameter("@SILowerTierSub", SqlDbType.VarChar, 255)).Value = item.SILowerTierSub;

                                        cmd.Parameters.Add(new SqlParameter("@SILowerTierSubOld", SqlDbType.VarChar, 255)).Value = item.SILowerTierSubOld;
                                        if (item.SIHours == 0)
                                            {
                                                item.SIHours = null;
                                            }
                                        cmd.Parameters.Add(new SqlParameter("@SIHours", SqlDbType.Decimal)).Value = item.SIHours;

                                        cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                                        cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 84;

                                        try

                                        {

                                            if (con.State != ConnectionState.Open)

                                                con.Open();

                                            cmd.ExecuteNonQuery();
                                            showMessageString = new
                                            {
                                                param1 = 200,
                                                param2 = "success"

                                            };

                                        }
                                        catch (Exception ex)
                                        {
                                            showMessageString = new
                                            {
                                                param1 = 404,
                                                param2 = ex.Message
                                            };

                                        }

                                        finally

                                        {

                                            if (con.State != ConnectionState.Closed)

                                                con.Close();

                                        }

                                    }
                                }

                            }
                            return Json(showMessageString, JsonRequestBehavior.AllowGet);

                          }
                            showMessageString = new
                            {
                                param1 = 200,
                                param2 = "success"

                            };
                            return Json(showMessageString, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                        }


                    //break;


                    case "No Hours For This Job":
                        using (var con = new SqlConnection

                        (ConfigurationManager.ConnectionStrings["App_SubcontractorHours"].ConnectionString))

                        {

                            var cmd = new SqlCommand("SubcontractorHours2018.SubcontractorHours", con);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("@SHMCU", SqlDbType.VarChar)).Value = MMMCU;

                            cmd.Parameters.Add(new SqlParameter("@SHRequisitionDate", SqlDbType.Date)).Value = SHRequisitionDate;

                            cmd.Parameters.Add(new SqlParameter("@SHStatus", SqlDbType.TinyInt)).Value = 30;

                            cmd.Parameters.Add(new SqlParameter("@SHRemark", SqlDbType.VarChar)).Value = SHRemark;

                            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                            cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 71;

                            try

                            {

                                if (con.State != ConnectionState.Open)

                                    con.Open();

                                cmd.ExecuteNonQuery();

                                showMessageString = new
                                {
                                    param1 = 200,
                                    param2 = "Skipped"

                                };



                            }
                            catch (Exception ex)
                            {
                                showMessageString = new
                                {
                                    param1 = 404,
                                    param2 = ex.Message
                                };

                            }


                            finally

                            {

                                if (con.State != ConnectionState.Closed)

                                    con.Close();

                            }

                        }

                        break;

                }

                List<spSubcontractorHours_Result> r1 = db.spSubcontractorHours(myshort, myid, MMMCU, SHRequisitionDate, false).ToList();
                if (r1.Count == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    return View(r1);
                }



            }

        }



    }
}