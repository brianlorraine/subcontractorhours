using System;
using System.ComponentModel.DataAnnotations;

namespace SubcontractorHours.Models
{


    public partial class spSubcontractorHours_Result
    {
        public Nullable<System.DateTime> SHRequisitionDate { get; set; }
        public string MMRegionGroup { get; set; }
        public string ENDescription { get; set; }
        public string MMRegion { get; set; }
        public string jobdescription { get; set; }
        public string MMMCU { get; set; }
        public short MMKeyType { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> SHTotalHours { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> SHTotalHoursJTD { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> SHTotalHoursJTDSub { get; set; }
        [Required(ErrorMessage = "Sub is required")]
        public string SILowerTierSub { get; set; }
        [Required(ErrorMessage = "Subcontractor number is required")]
        public string SILowerTierSubOld { get; set; }
        [Required(ErrorMessage = "Subcontractor name is required")]
        public int SISubcontractor { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> SIHours { get; set; }
        public Nullable<decimal> SIHoursOld { get; set; }
        [Required(ErrorMessage = "Subcontractor name is required")]
        public string abalph { get; set; }
        public string SHStatusDescription { get; set; }
        public Nullable<System.DateTime> SHDateChange { get; set; }
        public Nullable<byte> SHStatus { get; set; }
        public string SHRemark { get; set; }
        public Boolean QCF03K { get; set; }
    }
}
