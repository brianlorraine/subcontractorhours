﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;

namespace SubcontractorHours.Controllers
{
    public class KPIController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (actionName != "Login" || TempData.Peek("KeyType") == null)
            {

                if (Request.IsAuthenticated == false)
                {
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage(); 
                }

                if (Session["KPISystemUserID"] == null)
                {
                    //INT\BRLORRAI
                    string myname = System.Web.HttpContext.Current.User.Identity.Name;
                    KpiSecurity oKPISecurity = new KpiSecurity(myname, (Session["KPISystemIdentityAlt"] == null) ? "" : Session["KPISystemIdentityAlt"].ToString());
                    

                    if (oKPISecurity.UserID == null)
                    {
                        ViewBag.Error = "Your user account is disabled in KPI System.<BR><BR>Please contact the Bethesda Help Desk at extention 8157.";
                        
                    }
                    else
                    {
                        Session["KPISystemUserID"] = oKPISecurity.UserID;
                        Session["KPISystemUserName"] = oKPISecurity.FullName;
                        Session["KPISystemUserEmailAddress"] = oKPISecurity.EmailAddress;
                        Session["KPISystemUserApplication"] = oKPISecurity.Applications;

                        

                    }
                }
            }
        }
    }
}