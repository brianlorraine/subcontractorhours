﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Configuration;
using System.Reflection;

namespace SubcontractorHours
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(decimal), new SubcontractorHours.Models.Binders.DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new SubcontractorHours.Models.Binders.DecimalModelBinder());

            var fieldInfo = typeof(ConfigurationElementCollection).GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(ConfigurationManager.ConnectionStrings, false);

                ConnectionStringSettings myDB = new ConnectionStringSettings("App_KPI_SubcontractorHoursEntities", ConfigurationManager.AppSettings["App_KPI_SubcontractorHoursEntities"]);
                myDB.ProviderName = "System.Data.EntityClient";
                ConfigurationManager.ConnectionStrings.Add(myDB);

                ConnectionStringSettings myDB1 = new ConnectionStringSettings("App_KPI_SubcontractorHoursEntitiesSub", ConfigurationManager.AppSettings["App_KPI_SubcontractorHoursEntitiesSub"]);
                myDB1.ProviderName = "System.Data.EntityClient";
                ConfigurationManager.ConnectionStrings.Add(myDB1);

                ConnectionStringSettings myDB4 = new ConnectionStringSettings("App_SubcontractorHours", ConfigurationManager.AppSettings["App_SubcontractorHours"]);
                myDB4.ProviderName = "System.Data.SqlClient";
                ConfigurationManager.ConnectionStrings.Add(myDB4);

            }
        }
    }
}
